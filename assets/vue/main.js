import Vue from 'vue';
import App from './App';
import { router } from './routes/router';
import vuetify from './plugins/vuetify';
import { i18n } from './plugins/i18n';
import '../css/app.css';

new Vue({
  vuetify,
  router,
  i18n,
  render: h => h(App)
}).$mount('#app');
