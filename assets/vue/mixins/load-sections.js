// Utilities
/*
import {
  camelCase,
  upperFirst,
} from 'lodash'
*/
export default function (sections = []) {
  return {
    name: 'LoadSections',

    // Iterates the provide list of components
    // and returns a function that returns a
    // Promise.
    components: sections.reduce((accumulator, current) => {
      const name = current // upperFirst(camelCase(current))

      accumulator[`Section${name}`] = () => import(`../components/${name}.vue`)

      return accumulator
    }, {}),

    data: () => ({ sections }),
  }
}
