export default {
  message: {
    hello: "Hello world ! (i18n)",
    word: 'Say "{msg}" to mommy',
    basket: "nothing | only one item | {count} items",
  },
  menu: {
    home: "Home",
    contact: "Contact",
    account: "Account",
    login: "Login",
    other: "Other",
  },
  error: {
    // provided in English
  },
};
