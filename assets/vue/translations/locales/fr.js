export default {
  message: {
    hello: "Salut la Terre ! (i18n)",
    word: 'Dit "{msg}" à maman',
    basket: "rien | qu'un seul élément | {count} élements",
  },
  menu: {
    home: "Accueil",
    contact: "Contact",
    account: "Compte",
    login: "Connexion",
    other: "Autre",
  },
  error: {
    "This value should not be blank.": "Cette valeur ne doit pas être vide.",
    "This password has been leaked in a data breach, it must not be used. Please use another password.":
      "Ce mot de passe a fait l'objet d'une fuite lors d'une violation de données, il ne doit pas être utilisé. Veuillez utiliser un autre mot de passe.",
    "This value is not a valid email address.":
      "Cette valeur n'est pas une adresse email valide.",
  },
};
