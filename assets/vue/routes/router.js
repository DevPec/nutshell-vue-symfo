import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export const router = new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      component: () => import("../layout/MainLayout.vue"),
      children: [
        {
          path: "",
          name: "Home",
          component: () => import("../views/Home.vue"),
        },
        {
          path: "contact",
          name: "Contact",
          component: () => import("../views/Contact.vue"),
        },
        {
          path: "login",
          name: "Login",
          component: () => import("../views/Login.vue"),
        },
        {
          path: "*",
          name: "NotFound",
          component: () => import("../views/NotFound.vue"),
        },
      ],
    },
  ],
});
