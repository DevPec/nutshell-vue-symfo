import Vue from 'vue'
import VueI18n from 'vue-i18n'
import messages from "../translations";

Vue.use(VueI18n);

let locale = navigator.language.match(/^[a-z]{2}/);

export const i18n = new VueI18n({
    fallbackLocale: 'en',
    locale: locale,
    messages
});
