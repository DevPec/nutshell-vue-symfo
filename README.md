# Nutshell Project - Vue.JS Symfony

I wanted to start a project using Vue.Js with the best backend framework, Symfony.

Stay tune and have fun looking for this project that contain what every website have : a page with
a **menu**, a **login** system, an **admin page** and an **internationalisation** system. /play greatjobv

Support my projects on [![reward with tipee](https://fr.tipeee.com/img/tipeee-logo-com.png "Click me !")](https://fr.tipeee.com/devpec-gitlab/)
. It's [DevPec GitLab](https://fr.tipeee.com/devpec-gitlab/) !

## Summary

1. [Content](#content)
   1. [Stack](#stack)
   1. [Components](#components)
1. [How to use it](#how-to-use-it) :point_left:
   1. [Requirements](#requirements)
   1. [Actions](#actions)
   1. [Adapt few things](#adapt-few-things)
   1. [Extends with new pages](#extends-with-new-pages)
   1. [Dev Process](#dev-process)
1. [How did I build such a project](#how-did-i-build-such-a-project) :point_left:
   1. [The basement](#the-basement)
   1. [GET data from the API](#get-data-from-the-api)
   1. [Internationalization](#internationalization)
   1. [POST data in the API](#post-data-in-the-api)
   1. [Vuetify](#vuetify)
   1. [Security](#security)
   1. [Routing](#routing)

# Content

## Stack

The projet is build with `Symfony 5` in back as an API, featuring PHP (>7.3). The front uses `VueJS`.
Other convenient technologies are used, such as `webpack` to build my assets ans `npm` to manage JS packages and `composer` for the php ones.

## Components

As described shortly, this project is a sort of _boilerplate_ that involves common components of a website. Here is the list

- :cn: Ready to use **translation** system to allow internationalization
- :bookmark: Menu and **routing** system
- :pencil: Ready to use **form** system
- :lock: A **login** system, including forgotten password and cie
- :mortar_board: An **administration** system that allows back management

# How to use it

## Requirements

In order to be able to set up and follow the commands, you need to have installed on your developpement
computer the following things:

- PHP >7.3
- composer
- Symfony (CLI)
- npm

## Actions

Start by cloning the project for GitLab. Give it a star !

Type the following commands:

```bash
composer install
npm install
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
.\node_modules\.bin\encore dev
symfony server:start
```

And... that's it, you killed everything.

## Adapt few things

You then need to adapt few things :

- The database : In my project I use sqlite for more convenience (no other tools is required). But if
  you need to use something else, in order to match a different architecture, you're goo to go.
  Simply change into `.env` (or its local version) the folling keys : `DATABASE_URL`. And `APP_SECRET` by the way.

## Extends with new pages

### Front work

@TODO (Routing, Translations, ...)

### Back work

@TODO (Entity, Controllers, Services, ... )

## Dev Process

@TODO (Files Organization, Assets, Server...)

The Vue.JS application is located under `assets/js/`. The reason for this is because we use here webpack
for compiling assets, and JavaScripts files **are** assets files, even if it's a frontend application.
The Vue.JS application into the `main.js` file. The different components of the Vue.JS app are into `assets/js/components`.

The `src/` directory contains only the PHP files, necessary for the backend.

`.\node_modules\.bin\encore dev-server --hot`

`symfony server:start`

## Make it run

@TODO

## Known errors

Here is a list of errors that I faced during the development process, and the solution I found to fix it.

> entrypoints.json does not exist

\> You need to compile your assets using `encore build`

> Webpack Encore requires version ^8.0.0 of sass-loader

\> Upgrade the versin of the package with `npm install sass-loader@8.0.0`

> There is an issue with `node-fibers`

\> Appears on Debian, so to be able to make it install/update `sudo apt-get install g++ build-essential` and then `npm install fibers`

# How did I build such a project

In this part, I'll be covering the different commands and snippets of code I added to my project along
the different steps, to arrive to the current state of the project.

## The basement

A coffee and I started by :

```bash
symfony new nutshell-vue-symfo

composer require encore twig asset api maker migration

npm install -g @vue/cli

npm install --only=dev vue vue-loader vue-template-compiler

npm install sass-loader@^7.0.1 node-sass --save-dev

npm install
```

I edited the `webpack.config.js`

```javascript
var Encore = require("@symfony/webpack-encore");

if (!Encore.isRuntimeEnvironmentConfigured()) {
  Encore.configureRuntimeEnvironment(process.env.NODE_ENV || "dev");
}

Encore.setOutputPath("public/build/")
  .setPublicPath("/build")
  .cleanupOutputBeforeBuild()
  .enableBuildNotifications()
  .enableSourceMaps(!Encore.isProduction())
  .enableVersioning(Encore.isProduction())
  .splitEntryChunks()
  .enableSingleRuntimeChunk()

  .enableSassLoader()
  .enableVueLoader()

  .addEntry("app", "./assets/js/app.js")
  //.addEntry('page1', './assets/js/page1.js')

  // enables @babel/preset-env polyfills
  .configureBabelPresetEnv((config) => {
    config.useBuiltIns = "usage";
    config.corejs = 3;
  });

// uncomment if you use API Platform Admin (composer req api-admin)
//.enableReactPreset()
//.addEntry('admin', './assets/js/admin.js')

module.exports = Encore.getWebpackConfig();
```

I edited `DATABASE_URL` config in `.env`. For convenience here, I use sqlite database.

```
DATABASE_URL=sqlite:///%kernel.project_dir%/var/data.db
```

Then `config/packages/doctrine.yaml`

```yaml
doctrine:
  dbal:
    url: "%env(resolve:DATABASE_URL)%"
    server_version: "5.7"
  orm:
    auto_generate_proxy_classes: true
    naming_strategy: doctrine.orm.naming_strategy.underscore_number_aware
    auto_mapping: true
    mappings:
      App:
        is_bundle: false
        type: annotation
        dir: "%kernel.project_dir%/src/Entity"
        prefix: 'App\Entity'
        alias: App
```

```bash
php bin/console make:controller
> MainController
```

Then `config/routes.yaml`

```yaml
index:
  path: /
  controller: App\Controller\MainController::index
```

I changed `MainController.php` to this

```php
<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MainController extends AbstractController
{

    public function index()
    {
        return $this->render('main/index.html.twig');
    }
}
```

Then changed `main/index.html.twig` to this

```html
{% extends 'base.html.twig' %} {% block title %}Nutshell{% endblock %} {% block
body %}
<div id="app">
  Vue.JS App didn't load
</div>
{% endblock %}
```

and the **most important** into your `base.html.twig` use the `encore_entry_script_tags`, otherwise the
Vue application will not run with a simple `<script src="app.js"/>`. You are welcome.

```twig
{% block javascripts %}
    {{ encore_entry_script_tags('app') }}
{% endblock %}
```

Then I compiled my assets with

```bash
.\node_modules\.bin\encore dev-server --hot
# or
.\node_modules\.bin\encore dev --watch
```

## GET data from the API

Initiated the database (after having installed all drivers required for sqlite)

```bash
php bin/console doctrine:database:create
php bin/console make:entity
php bin/console make:migration
php bin/console doctrine:migrations:migrate
symfony server:start
```

Then I was able to access to my admin page on [localhost:8000/api](http://127.0.0.1:8000/api)
Create some fake users

To be able to call my api data from the Vue app,
I needed to install axios like this

```bash
npm install axios
```

I created the following component

```vue
<template>
  <div class="users">
    <h1>Your users</h1>
    <ul v-if="users.length">
      <li v-for="user in users">{{ user.name }} - {{ user.mail }}</li>
    </ul>
    <p v-else>No user found.</p>
  </div>
</template>

<script>
const axios = require("axios");

export default {
  name: "users",
  data() {
    return {
      users: [],
    };
  },
  mounted() {
    axios
      .get("https://localhost:8000/api/users?page=1")
      .then((response) => (this.users = response.data["hydra:member"]));
  },
};
</script>
```

## Internationalization

To add the i18n translation system, I executed

```bash
npm install vue-i18n
npm install @vue/cli -g
vue add i18n
```

Then I configured the plugin in `js/plugins/i18n.js`

```javascript
import Vue from "vue";
import VueI18n from "vue-i18n";
import messages from "../translations";

Vue.use(VueI18n);

let locale = navigator.language.match(/^[a-z]{2}/);

export const i18n = new VueI18n({
  fallbackLocale: "en",
  locale: locale,
  messages,
});
```

Then I set up the translations files
and included them in the `main.js` like this

```javascript
import "../css/app.css";
import Vue from "vue";
import App from "./App";
import { i18n } from "./plugins/i18n";

new Vue({
  el: "#app",
  i18n,
  template: "<App/>",
  components: {
    App,
  },
});
```

And now I can use the translations into my Vue components, with the tags:

```html
<h1>{{ $t('message.hello') }}</h1>
<p>{{ $t('message.word', {msg: this.message}) }}</p>
<p>{{ $tc('message.basket', this.number, {count: this.number}) }}</p>
```

## POST data in the API

To enable the validation of my entities, I run :
`composer require symfony/validator symfony/http-client`

Then I defined somme constraints in `AbstractUser` with the `@Assert` marker:

```php
/**
 * @Assert\NotBlank
 * @Assert\Email
 * @ORM\Column(type="string", length=255, unique=true)
 */
private $mail;

/**
 * @Assert\NotBlank
 * @ORM\Column(type="string", length=255)
 */
private $name;

/**
 * @Assert\NotBlank
 * @Assert\NotCompromisedPassword
 * @ORM\Column(type="string", length=255)
 */
private $password;
```

The I created the new Vue Component : `Poster.vue`, in which I put the following content to be able to post

```javascript
axios.post("https://localhost:8000/api/users", {
  name: this.name,
  password: this.password,
  mail: this.mail,
});
```

I also wanted to bind the errors I could receive to the corresponding field of the form.
To do that I implemented the function into the axios part :

```javascript
axios
    .post('https://localhost:8000/api/users', {
    // ...
    })
    .then(function (response) {
        currentObj.output = response;
    })
    .catch(function (error) {
        currentObj.output = error.response;
        console.log(error.response.data.violations);
        currentObj.updateErrors(error.response.data.violations);
    });
// ...
updateErrors(violations) {
    let currentObj = this;
    for (const violation of violations) {
        currentObj.$set(currentObj.errors, violation.propertyPath, violation.message);
    }
}
```

So in the corresponding html file, I adjusted with the following content :

```html
<input type="text" class="form-control" v-model="name" />
<div v-if="errors.name">{{ $t(`error["${errors.name}"]`) }}</div>

<input type="password" class="form-control" v-model="password" />
<div v-if="errors.password">{{ $t(`error["${errors.password}"]`) }}</div>

<input type="email" class="form-control" v-model="mail" />
<div v-if="errors.mail">{{ $t(`error["${errors.mail}"]`) }}</div>
```

And lastly, to be able to translate the error messages, I had to use the following mechanism, because I wanted
nested translation elements, and use the sentence from the API response as the json key.
The error messages are located under `error`.

```javascript
export default {
  // ...
  error: {
    "This value should not be blank.": "Cette valeur ne doit pas être vide.",
    // Place here all untranslated error messages
  },
};
```

So for the internationalization, the i18n corresponding is the following :

```html
{{ $t(`error["${errors.mail}"]`) }}
```

## Vuetify

To be able to use easily Vuetify in this project, I used it with the `vuetify-loader` package.

```cmd
npm install vuetify vuetify-loader -D
npm install sass sass-loader fibers deepmerge -D
```

Now I need to adapt few configurations.

- First of all, the webpack config in `webpack.config.js` : to add the loader plugin and aggregate the SassLoader with options

  ```javascript
  const VuetifyLoaderPlugin = require("vuetify-loader/lib/plugin");

  // ...

  Encore
    // ...
    .enableSassLoader((options) => {
      options.implementation = require("sass");
      options.fiber = require("fibers");
    })
    .addPlugin(new VuetifyLoaderPlugin());
  // ...
  ```

- Then I needed to allow the effective loading in my front vues

  ```twig
  {% block stylesheets %}
     {{ encore_entry_link_tags('app') }}
  {% endblock %}
  ```

- And in my `main.js` I added the corresponding line to use vuetify in my `Vue` object

  ```javascript
  import Vuetify from 'vuetify/lib'

  // ...

  new Vue({
    el: '#app',
    vuetify,

  // ...
  ```

At this point I was able to transform my application to use

For instance, I could replace in `User.vue` the `<ul>` tag by a combo `<v-skeleton-loader>` and `<v-list>`.

The real problems I got where the errors binding on the form (and still be able to translate them) and the icon loading.

For the first one, the form and error translations I changed the `updateErros()` function with :

```javascript
updateErrors(violations) {
    let currentObj = this;
    for (const violation of violations) {
        currentObj.$set(currentObj.errors, violation.propertyPath, this.$t(`error["${violation.message}"]`));
    }
}
```

And this way it sets directly into the fields if an error sis found by the API

```html
<v-text-field
  v-model="name"
  :error-messages="errors.name"
  :count="255"
  label="Name"
  required
  @input="$v.name.$touch()"
  @blur="$v.name.$touch()"
></v-text-field>
```

For the icons I had to load in every vue file the icons I use by

```javascript
    import {mdiPencil} from '@mdi/js';

    export default {
        name: 'poster',
        data() {
            return {
               // ...
                icons: {
                    pencil: mdiPencil,
                }
                // ...
```

and use it like this

```html
<v-icon medium>{{icons.pencil}}</v-icon>
```

## Update

```
npm update
compoer update
composer update "symfony/*" --with-all-dependencies
```

Some code refaction to match new standards in `webpack.config.js`

```
.enableSassLoader(options => {
        options.implementation = require('sass')
        ~~options.fiber = require('fibers')~~

    })
```

and in `App.vue`

```
~~<v-content>~~ <v-main>
    // ...
~~</v-content>~~ </v-main>
```

## Routing

@DOING

First add the recommanded router for Vue with

```
npm install vue-router
```

Then I edited the `main.js` to load it and add it to Vue.
Now I have in my `App.vue` the following code to actually make the content based on what the router tells.

```vue
<v-main>
    <router-view/>
</v-main>
```

So I created the `routes/router.js` that is responsible to load the content inside the balise depending on the route match. All the future routeswill be defined here and "attached" to a component (that will be located under `/views`for the different pages and under `/layout` for the layout).

So now, I create a bunch of component for my different pages : `views/Home.vue`, `views/NotFound.vue` and `layout/MainLayout.vue`

I also made a dynamic menu, responsive and extendable.
Inside `/components/MainMenu.vue` I used lists with label, route and icons and thoose are redered into a v-list that will allow me to easily extends it in the future projects.

I choosed to make it translatable and as you can see I made into my ttranslation files a sublevel for menu.

There is also a dynamic system for loading the different views. This system uses a mixin located in `/mixins/load-sections.js` that allows to take a list of section into `/components` and render it. But I don't know if I will keep this system in the future as it is quite complicated for not so much...

So to understand the rendering I made the schema bellow :

```
App.vue --> router-view --> MainLayout = main-app-bar + router-view + main-footer
                                                            !!
                                                Sections + Section Loader
```

And as the time was passing, I was adding more and more components...

I had a problem with my icons so I fixed it with the following method :

```
npm install @mdi/font -D
```

and inside the `vuetify.js`

```javascript
import '@mdi/font/css/materialdesignicons.css'

icons: {
    iconfont: 'mdi'
    // ...
```

I also wanted to customise the loader beafore the Vue app is actually loaded. So I used inside my template a div and pure css loader to make the wanted effect. It's almost an easter egg, knowing that the app should load quicker... But we never know.

## Security

@TODO

## Administration Page

@TODO

## Refacto

@TODO

- one level translations ?
- icons download
- ...

## SEO

(analytics and metadata)
@TODO

## Dark mode

@TODO
